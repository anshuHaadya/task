//
//  EmpStore.m
//  Task
//
//  Created by iOS on 15/11/20.
//

#import "EmpStore.h"
#import "AppDelegate.h"
@implementation EmpStore
+(NSMutableArray*)sharedEmployees{
   return ((AppDelegate*)[UIApplication sharedApplication].delegate).employees;
}

@end
