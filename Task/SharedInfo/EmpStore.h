//
//  EmpStore.h
//  Task
//
//  Created by iOS on 15/11/20.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface EmpStore : NSObject
+(NSMutableArray*)sharedEmployees;

@end


NS_ASSUME_NONNULL_END
