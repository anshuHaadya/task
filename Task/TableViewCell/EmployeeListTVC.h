//
//  EmployeeListTVC.h
//  Task
//
//  Created by iOS on 15/11/20.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EmployeeListTVC : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblEmpName;
@property (weak, nonatomic) IBOutlet UILabel *lblEmpId;

@end

NS_ASSUME_NONNULL_END
