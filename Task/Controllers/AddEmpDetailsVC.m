//
//  AddEmpDetailsVC.m
//  Task
//
//  Created by iOS on 15/11/20.
//

#import "AddEmpDetailsVC.h"
#import "UIView+Additions.h"
#import "EmployeeModel.h"
#import "UITextField+Validations.h"
#import "UITextView+Validations.h"
#import "EmpStore.h"


@interface AddEmpDetailsVC ()

@end

@implementation AddEmpDetailsVC

#pragma mark - ViewLife Cycles
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCustomUI];
    self.txtCollection = @[self.tfEmpName, self.tfEmpId, self.tfDepartment, self.tfempSalary, self.tvDescription];
}
#pragma mark - Custom Methods
-(void)setCustomUI{
    [_tfEmpName setBorderForColor:UIColor.blackColor width:0.1 radius:10];
    [_tfDepartment setBorderForColor:UIColor.blackColor width:0.1 radius:10];
    [_tfEmpId setBorderForColor:UIColor.blackColor width:0.1 radius:10];
    [_tfempSalary setBorderForColor:UIColor.blackColor width:0.1 radius:10];
    [_tvDescription setBorderForColor:UIColor.blackColor width:0.1 radius:10];

}
-(BOOL)checkIfAllFieldsAreFilled{
    BOOL isValid = YES;
    // If any of the text fields in the collection does not have data then say no.
    for(UITextField *tf in self.txtCollection)
    {
        if( ![tf isValid] ){
            isValid = NO;
        }
    }
    if( isValid )
        return YES;
    return NO;
}

#pragma mark - Button Actions
- (IBAction)onClickSave:(UIButton *)sender {
    if( [self checkIfAllFieldsAreFilled] ){
        EmployeeModel *emp = [[EmployeeModel alloc]init];
        emp.empName = self.tfEmpName.text;
        emp.empDepartment = self.tfDepartment.text;
        emp.empId = self.tfEmpId.text;
        emp.empDetails = self.tvDescription.text;
        emp.empSalary = self.tfempSalary.text;
        NSMutableArray *sharedEmp = [EmpStore sharedEmployees];
        [sharedEmp addObject:emp];
        // Clear the text in all the text fields in the collection
        for(UITextField *tf in self.txtCollection)
        {
            tf.text = nil;
        }

        
        [self performSegueWithIdentifier:@"empList" sender:self];

    } else{
        UIAlertController * alert = [UIAlertController
                                        alertControllerWithTitle:@"Please enter all the fields"
                                        message:@""
                                        preferredStyle:UIAlertControllerStyleAlert];

           UIAlertAction* okButton = [UIAlertAction
                                      actionWithTitle:@"Ok"
                                      style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          //Handle no, thanks button
                                      }];

           [alert addAction:okButton];

           [self presentViewController:alert animated:YES completion:nil];

    }

}
#pragma mark - TextField DelegateMethods
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    CGRect rc = [textField bounds];
    rc = [textField convertRect:rc toView:_scrollView];
    rc.origin.x = 0 ;
    rc.origin.y += 60 ;
    
    rc.size.height = 400;
    [self.scrollView scrollRectToVisible:rc animated:YES];

}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField.tag == 0){
        [_tfDepartment becomeFirstResponder];
    } else if (textField.tag == 1){
        [_tfEmpId becomeFirstResponder];
    } else if (textField.tag == 2){
        [_tfempSalary becomeFirstResponder];
    } else{
        [_tvDescription becomeFirstResponder];
    }
    
    CGRect rc = [textField bounds];
    rc = [textField convertRect:rc toView:_scrollView];
    rc.origin.x = 0 ;
    rc.origin.y += 60 ;
    
    rc.size.height = 400;
    [self.scrollView scrollRectToVisible:rc animated:YES];
    [textField resignFirstResponder];
    return  true;
    
}
#pragma mark - TextView DelegateMethods
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]) {
        
        [textView resignFirstResponder];
        // Return FALSE so that the final '\n' character doesn't get added
        return NO;
    }
    // For any other character return TRUE so that the text gets added to the view
    return textView.text.length + (text.length - range.length) <= 50;
}
- (void)textViewDidBeginEditing:(UITextView *)textView{
    CGRect rc = [textView bounds];
    rc = [textView convertRect:rc toView:_scrollView];
    rc.origin.x = 0 ;
    rc.origin.y += 60 ;
    
    rc.size.height = 600;
    [self.scrollView scrollRectToVisible:rc animated:YES];
    
}
@end
