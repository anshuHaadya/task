//
//  EmpDetailsVC.h
//  Task
//
//  Created by iOS on 15/11/20.
//

#import <UIKit/UIKit.h>
#import "EmployeeModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface EmpDetailsVC : UIViewController
- (IBAction)OnclickBack:(id)sender;

@property EmployeeModel *emp;
@property (weak, nonatomic) IBOutlet UITextField *empName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmpDept;
@property (weak, nonatomic) IBOutlet UITextField *txtEmpId;
@property (weak, nonatomic) IBOutlet UITextField *txtEmpSalary;
@property (weak, nonatomic) IBOutlet UITextView *txtEmpDetails;

@end

NS_ASSUME_NONNULL_END
