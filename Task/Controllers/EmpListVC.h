//
//  EmpListVC.h
//  Task
//
//  Created by iOS on 15/11/20.
//

#import <UIKit/UIKit.h>
#import "EmployeeModel.h"
#import "EmpStore.h"
#import "EmpDetailsVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface EmpListVC : UIViewController
@property NSMutableArray *employeeList;
@property NSInteger selectedIndex;
@property NSMutableArray *searchemployeeList;
@property NSMutableArray *searchemployeeid;
@property (assign) BOOL isFiltered;
@property (weak, nonatomic) IBOutlet UISearchBar *search;

@end

NS_ASSUME_NONNULL_END
