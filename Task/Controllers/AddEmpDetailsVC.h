//
//  AddEmpDetailsVC.h
//  Task
//
//  Created by iOS on 15/11/20.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AddEmpDetailsVC : UIViewController <UITextFieldDelegate, UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *tfEmpName;
@property (weak, nonatomic) IBOutlet UITextField *tfDepartment;
@property (weak, nonatomic) IBOutlet UITextField *tfEmpId;
@property (weak, nonatomic) IBOutlet UITextField *tfempSalary;
@property (weak, nonatomic) IBOutlet UITextView *tvDescription;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property NSArray *txtCollection;

- (IBAction)onClickSave:(UIButton *)sender;

@end

NS_ASSUME_NONNULL_END
