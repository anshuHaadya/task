//
//  EmpDetailsVC.m
//  Task
//
//  Created by iOS on 15/11/20.
//

#import "EmpDetailsVC.h"

@interface EmpDetailsVC ()

@end

@implementation EmpDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
}
- (IBAction)OnclickBack:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)setUp{
    self.txtEmpDept.text = self.emp.empDepartment;
    self.txtEmpDetails.text = self.emp.empDetails;
    self.txtEmpId.text = self.emp.empId;
    self.empName.text = self.emp.empName;
    self.txtEmpSalary.text = self.emp.empSalary;
}


@end
