//
//  EmpListVC.m
//  Task
//
//  Created by iOS on 15/11/20.
//

#import "EmpListVC.h"
#import "EmployeeListTVC.h"
#import "EmpStore.h"
@interface EmpListVC ()
@property (weak, nonatomic) IBOutlet UITableView *tblView;
- (IBAction)onClickBack:(id)sender;
@end

@implementation EmpListVC

#pragma mark - ViewLife Cycles
- (void)viewDidLoad {
    [super viewDidLoad];
    self.isFiltered = NO;
    self.employeeList = [EmpStore sharedEmployees];
    _searchemployeeList = [[NSMutableArray alloc]init];

}
#pragma mark - TableView DelegateMethods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.isFiltered == YES){
        return self.searchemployeeList.count;
    }else{
        return self.employeeList.count;
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    EmployeeListTVC *cell = [tableView dequeueReusableCellWithIdentifier:@"EmployeeListTVC"];
    if (self.isFiltered == YES){
        EmployeeModel *emp = self.searchemployeeList[indexPath.row];
        cell.lblEmpName.text = emp.empName;
        cell.lblEmpId.text = emp.empId;
    
    } else{
        EmployeeModel *emp = self.employeeList[indexPath.row];
        cell.lblEmpName.text = emp.empName;
        cell.lblEmpId.text = emp.empId;

    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.selectedIndex = indexPath.row;
    [self performSegueWithIdentifier:@"empdetails" sender:self];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    return 100;
}
#pragma mark - Segue
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    EmpDetailsVC *details = segue.destinationViewController;
    details.emp = self.employeeList[self.selectedIndex];
}
#pragma mark - search Bar
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    searchBar.text = searchText.lowercaseString;
 if(searchText.length == 0){
     _isFiltered = NO;
 }else{
     _isFiltered = YES;
     [_searchemployeeList removeAllObjects];

     NSPredicate *empName = [NSPredicate predicateWithFormat:
                               @"empName CONTAINS[c] %@", searchText];
     NSArray *empNameArray = [_employeeList filteredArrayUsingPredicate:empName];
        [_searchemployeeList addObjectsFromArray:empNameArray];
    }
    [self.tblView reloadData];

 }
#pragma mark - Button Actions
 - (IBAction)onClickBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];

}
@end
