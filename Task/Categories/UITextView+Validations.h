//
//  UITextView+Validations.h
//  Task
//
//  Created by iOS on 15/11/20.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITextView (Validations)
-(BOOL)isValid;

@end

NS_ASSUME_NONNULL_END
