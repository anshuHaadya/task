//
//  UIView+Additions.h
//  Task
//
//  Created by iOS on 15/11/20.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (Additions)
- (void)setBorderForColor:(UIColor *)color
                    width:(float)width
                   radius:(float)radius;

@end


NS_ASSUME_NONNULL_END
