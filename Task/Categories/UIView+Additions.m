//
//  UIView+Additions.m
//  Task
//
//  Created by iOS on 15/11/20.
//

#import "UIView+Additions.h"

@implementation UIView (Additions)
- (void)setBorderForColor:(UIColor *)color
                    width:(float)width
                   radius:(float)radius
{
    self.layer.cornerRadius = radius;
    self.layer.masksToBounds = YES;
    self.layer.borderColor = [color CGColor];
    self.layer.borderWidth = width;
}

@end
