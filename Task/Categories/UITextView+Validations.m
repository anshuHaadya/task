//
//  UITextView+Validations.m
//  Task
//
//  Created by iOS on 15/11/20.
//

#import "UITextView+Validations.h"

@implementation UITextView (Validations)
-(BOOL)isValid
{
    if( self.text != nil && self.text.length > 0 )
    {
        return YES;
    }
    return NO;
}

@end
