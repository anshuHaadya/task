//
//  EmployeeModel.m
//  Task
//
//  Created by iOS on 15/11/20.
//

#import "EmployeeModel.h"

@implementation EmployeeModel
-(NSString*)descritption{
    return [NSString stringWithFormat:@"Name : %@, Dept : %@, Id : %@, Salary : %@, Details : %@", self.empName, self.empDepartment, self.empId, self.empSalary, self.empDetails];
}

@end
