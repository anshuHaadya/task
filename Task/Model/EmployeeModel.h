//
//  EmployeeModel.h
//  Task
//
//  Created by iOS on 15/11/20.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface EmployeeModel : NSObject
@property NSString *empName, *empDepartment, *empId, *empSalary, *empDetails;

@end

NS_ASSUME_NONNULL_END
