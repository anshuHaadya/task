//
//  SceneDelegate.h
//  Task
//
//  Created by iOS on 15/11/20.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

